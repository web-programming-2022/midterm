import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "../types/product";

export const useProductStore = defineStore("product", () => {
  const products = ref(
    Array.from(Array(100).keys()).map((item) => {
      return {
        id: item,
        name: "Product " + (item + 1),
        price: (Math.floor(Math.random() * 100) + 1) * 10,
      };
    })
  );

  const sumPrice = ref(0);

  function addProducts(id: number, name: string, price: number): void {
    productLists.value.push({ id, name, price });
    sumPrice.value += price;
  }

  const productLists = ref<Product[]>([]);

  return { products, productLists, addProducts, sumPrice };
});
